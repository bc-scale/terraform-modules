variable "subnet_name" {
  type        = string
  description = "subnet name"
}

variable "instance_name" {
  type        = string
  description = "instance name"
}

variable "compartment_id" {
  type        = string
  description = "ocid of compartment id"

  default = "ocid1.tenancy.oc1..aaaaaaaal7fowf6dq25plx67ldx6zh2bcxddj6zj7p6otiqcfxaoeyswvyra"
}

variable "vcn_name" {
  type        = string
  description = "vnc name"
}

variable "shape" {
  type        = string
  description = "instance shape"

  default = "VM.Standard.E2.1.Micro"
}

variable "availability_domain" {
  type        = string
  description = "availability domain of instance"

  default = "IgTv:AP-SEOUL-1-AD-1"
}

variable "boot_volume_size" {
  type        = number
  description = "boot volume size"

  default = 50
}

variable "ssh_public_key" {
  type        = list
  description = "ssh public key for authorized keys"

  default = []
}

variable "image_name" {
  type        = string
  description = "exact image name"

  default = "Oracle-Linux-7.8-2020.06.30-0"
}

variable "reserve_public_ip" {
  type        = bool
  description = "reserve public ip"

  default = false
}
