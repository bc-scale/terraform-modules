resource "oci_core_instance" "instance" {
  compartment_id = var.compartment_id

  display_name        = var.instance_name
  shape               = var.shape
  availability_domain = var.availability_domain

  create_vnic_details {
    subnet_id        = data.oci_core_subnets.subnets.subnets[0].id
    assign_public_ip = false
  }

  source_details {
    source_id   = data.oci_core_images.images.images[0].id
    source_type = "image"

    boot_volume_size_in_gbs = var.boot_volume_size
  }

  shape_config {
    ocpus = 1
  }

  launch_options {
    boot_volume_type        = "PARAVIRTUALIZED"
    network_type            = "PARAVIRTUALIZED"
    remote_data_volume_type = "PARAVIRTUALIZED"
  }

  metadata = {
    ssh_authorized_keys = length(var.ssh_public_key) > 0 ? join("\n", var.ssh_public_key) : file("~/.ssh/authorized_keys")
  }
}

resource "oci_core_public_ip" "public_ip" {
  count = var.reserve_public_ip ? 1 : 0

  compartment_id = var.compartment_id

  display_name  = var.instance_name
  lifetime      = "RESERVED"
  private_ip_id = data.oci_core_private_ips.private_ip.private_ips[0].id
}
