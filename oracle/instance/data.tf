data "oci_core_subnets" "subnets" {
  compartment_id = var.compartment_id
  vcn_id         = data.oci_core_vcns.vcns.virtual_networks[0].id

  display_name = var.subnet_name
}

data "oci_core_vcns" "vcns" {
  compartment_id = var.compartment_id

  display_name = var.vcn_name
}

data "oci_core_images" "images" {
  compartment_id = var.compartment_id

  display_name = var.image_name

  #operating_system         = "Oracle Linux"
  #operating_system_version = "7.8"

  sort_by    = "TIMECREATED"
  sort_order = "DESC"
}

data "oci_core_private_ips" "private_ip" {
  subnet_id  = data.oci_core_subnets.subnets.subnets[0].id
  ip_address = oci_core_instance.instance.private_ip
}
