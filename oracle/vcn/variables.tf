variable "vcn_cidr_block" {
  type        = string
  description = "cidr block of vcn"
}

variable "vcn_name" {
  type        = string
  description = "display name of vcn"
}

variable "subnet_cidr_block" {
  type        = string
  description = "cidr block of subnet"
}

variable "compartment_id" {
  type        = string
  description = "ocid of compartment id"

  default     = "ocid1.tenancy.oc1..aaaaaaaal7fowf6dq25plx67ldx6zh2bcxddj6zj7p6otiqcfxaoeyswvyra"
}

variable "subnet_name" {
  type        = string
  description = "subnet display name"
}
