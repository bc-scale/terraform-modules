resource "oci_core_vcn" "vcn" {
  compartment_id = var.compartment_id

  display_name = var.vcn_name
  cidr_block   = var.vcn_cidr_block
}

resource "oci_core_subnet" "subnet" {
  compartment_id = var.compartment_id

  vcn_id                     = oci_core_vcn.vcn.id
  display_name               = var.subnet_name
  cidr_block                 = var.subnet_cidr_block
  route_table_id             = oci_core_route_table.route_table.id
  security_list_ids          = [oci_core_security_list.security_list.id]
  prohibit_public_ip_on_vnic = false
}

resource "oci_core_internet_gateway" "internet_gateway" {
  compartment_id = var.compartment_id

  vcn_id       = oci_core_vcn.vcn.id
  display_name = var.subnet_name
  enabled      = true
}

resource "oci_core_route_table" "route_table" {
  compartment_id = var.compartment_id

  vcn_id       = oci_core_vcn.vcn.id
  display_name = var.subnet_name

  route_rules {
    cidr_block        = "0.0.0.0/0"
    network_entity_id = oci_core_internet_gateway.internet_gateway.id
  }
}

resource "oci_core_security_list" "security_list" {
  compartment_id = var.compartment_id

  vcn_id       = oci_core_vcn.vcn.id
  display_name = var.subnet_name

  egress_security_rules {
    protocol    = "all"
    destination = "0.0.0.0/0"
  }

  ingress_security_rules {
    protocol    = 6
    source      = "0.0.0.0/0"
    description = "custom ssh port"
    tcp_options {
      min = 2225
      max = 2225
    }
  }

  ingress_security_rules {
    protocol    = 6
    source      = "0.0.0.0/0"
    description = "dns"
    tcp_options {
      min = 53
      max = 53
    }
  }

  ingress_security_rules {
    protocol    = 17
    source      = "0.0.0.0/0"
    description = "dns"
    udp_options {
      min = 53
      max = 53
    }
  }

  ingress_security_rules {
    protocol    = 6
    source      = "0.0.0.0/0"
    description = "http"
    tcp_options {
      min = 80
      max = 80
    }
  }


  ingress_security_rules {
    protocol    = 6
    source      = "0.0.0.0/0"
    description = "https"
    tcp_options {
      min = 443
      max = 443
    }
  }

}
