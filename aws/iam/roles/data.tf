data "aws_iam_policy_document" "assume_role_robot" {
  statement {
    effect = "Allow"

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::585882300414:user/lara",
        "arn:aws:iam::585882300414:user/leela",
        "arn:aws:iam::585882300414:user/mac-i9",
        "arn:aws:iam::585882300414:user/nuc",
        "arn:aws:iam::585882300414:user/poppy",
        "arn:aws:iam::585882300414:user/ripley",
      ]
    }

    actions = [
      "sts:AssumeRole",
    ]
  }
}

data "aws_iam_policy_document" "assume_role_human" {
  statement {
    effect = "Allow"

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::585882300414:user/waf",
      ]
    }

    actions = [
      "sts:AssumeRole",
    ]

    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"
      values = [
        true
      ]
    }
  }
}
