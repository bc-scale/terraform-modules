variable "human" {
  type        = bool
  description = "whether human or robot"
  default     = false
}

variable "name" {
  type        = string
  description = "name of role"
}

variable "policy_arn" {
  type        = string
  description = "policy arn to attach to role"
  default     = "arn:aws:iam::aws:policy/AWSDenyAll"
}

variable "assume_role_policy" {
  type        = string
  description = "assume role policy"
  default     = ""
}

variable "description" {
  type        = string
  description = "description of role"
  default     = ""
}
