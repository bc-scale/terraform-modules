resource "aws_iam_role" "main" {
  name        = var.name
  description = var.description

  assume_role_policy = var.assume_role_policy != "" ? var.assume_role_policy : var.human ? data.aws_iam_policy_document.assume_role_human.json : data.aws_iam_policy_document.assume_role_robot.json

  tags = local.tags
}

resource "aws_iam_role_policy_attachment" "main" {
  role       = aws_iam_role.main.name
  policy_arn = var.policy_arn
}
