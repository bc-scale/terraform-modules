variable "name" {
  type        = string
  description = "user name"
}

variable "groups" {
  type        = set(string)
  description = "list of group names"
}

variable "policies" {
  type        = set(string)
  description = "list of optional policies to attach"
  default     = []
}
