resource "aws_organizations_account" "main" {
  name  = var.name
  email = var.email

  iam_user_access_to_billing = "ALLOW"
  role_name                  = local.role_name

  parent_id = local.ou_map[var.parent_ou]

  tags = local.tags

  lifecycle {
    ignore_changes = [
      iam_user_access_to_billing,
      role_name,
    ]
    prevent_destroy = true
  }
}

resource "aws_iam_account_alias" "alias" {
  provider = aws.new_account

  account_alias = replace(lower(var.name), "_", "-")
}
