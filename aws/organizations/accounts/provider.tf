provider "aws" {
  alias = "new_account"

  profile = "waf-mgmt"
  region  = "us-west-1"

  assume_role {
    role_arn     = "arn:aws:iam::${aws_organizations_account.main.id}:role/${local.role_name}"
    session_name = "new_account_creation"
  }
}
