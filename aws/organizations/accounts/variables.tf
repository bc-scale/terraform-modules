variable "name" {
  type        = string
  description = "name of account"
}

variable "email" {
  type        = string
  description = "email address of root user"
}

variable "parent_ou" {
  type        = string
  description = "parent OU"
}
