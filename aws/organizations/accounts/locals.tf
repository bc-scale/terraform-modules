locals {
  role_name = "mgmt"

  ou_map = {
    for child in data.aws_organizations_organizational_units.ou.children : child.name => child.id
  }

  tags = {
    managed_by            = "terraform"
    terraform_module_name = abspath(path.root)
  }
}
