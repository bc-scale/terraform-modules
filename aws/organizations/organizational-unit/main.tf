resource "aws_organizations_organizational_unit" "main" {
  name      = var.name
  parent_id = data.aws_organizations_organization.main.roots[0].id

  tags = local.tags
}
