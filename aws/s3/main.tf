resource "aws_s3_bucket" "main" {
  bucket = var.bucket_name

  tags = {
    managed_by            = "terraform"
    terraform_module_name = "aws/s3"
  }
}

resource "aws_s3_bucket_policy" "main" {
  bucket = aws_s3_bucket.main.bucket
  policy = data.aws_iam_policy_document.main.json
}

resource "aws_s3_bucket_versioning" "main" {
  count = var.versioning ? 1 : 0

  bucket = aws_s3_bucket.main.bucket

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "main" {
  bucket = aws_s3_bucket.main.bucket

  rule {
    id     = "archive/expire/delete"
    status = "Enabled"
    filter {
      prefix = var.prefix
    }

    abort_incomplete_multipart_upload {
      days_after_initiation = 3

    }
    dynamic "expiration" {
      for_each = var.expire ? [1] : []

      content {
        days                         = var.archive ? 91 : 10
        expired_object_delete_marker = true
      }
    }

    noncurrent_version_expiration {
      noncurrent_days = var.archive ? 181 : 6
    }

    dynamic "noncurrent_version_transition" {
      for_each = var.archive ? [1] : []

      content {
        noncurrent_days = 1
        storage_class   = "DEEP_ARCHIVE"
      }
    }

    dynamic "transition" {
      for_each = var.archive ? [1] : []

      content {
        days          = 1
        storage_class = "DEEP_ARCHIVE"
      }
    }
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "main" {
  bucket = aws_s3_bucket.main.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_acl" "main" {
  bucket = aws_s3_bucket.main.bucket
  acl    = "private"
}
