variable "bucket_name" {
  type        = string
  description = "bucket name"
}

variable "expire" {
  type        = bool
  description = "expire objects"

  default = false
}

variable "archive" {
  type        = bool
  description = "archive objects"

  default = true
}

variable "prefix" {
  type        = string
  description = "lifecycle rule prefix"

  default = null
}

variable "versioning" {
  type        = bool
  description = "version objects in bucket"

  default = false
}
