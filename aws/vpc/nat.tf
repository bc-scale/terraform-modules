resource "aws_iam_role" "nat" {
  name               = "nat"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json

  tags = local.common_tags
}

resource "aws_iam_role_policy" "nat" {
  name   = "s3"
  role   = aws_iam_role.nat.id
  policy = data.aws_iam_policy_document.nat.json
}

resource "aws_iam_instance_profile" "nat" {
  name = "nat"
  role = aws_iam_role.nat.name
}

resource "aws_security_group" "nat" {
  name        = "nat gateway"
  description = "nat gateway sg"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "all from VPC"
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = [aws_vpc.main.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(local.common_tags,
    {
      Name = "nat gateway"
    }
  )
}

resource "aws_network_interface" "nat" {
  security_groups   = [aws_security_group.nat.id]
  source_dest_check = false
  subnet_id         = aws_subnet.public[0].id

  tags = merge(local.common_tags,
    {
      Name = "nat gateway"
    }
  )
}

resource "aws_launch_template" "main" {
  name_prefix = "nat"
  image_id    = data.aws_ami.amzn2.id
  user_data = base64encode(templatefile("${path.module}/nat_user_data.tftpl", {
    bootstrap_bucket = var.bootstrap_bucket,
  }))

  iam_instance_profile {
    arn = aws_iam_instance_profile.nat.arn
  }

  network_interfaces {
    network_interface_id = aws_network_interface.nat.id
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "nat gateway"
    }
  }

  tags = local.common_tags
}

resource "aws_autoscaling_group" "main" {
  name               = "nat"
  desired_capacity   = 1
  min_size           = 1
  max_size           = 1
  availability_zones = [aws_subnet.public[0].availability_zone]

  mixed_instances_policy {
    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.main.id
        version            = "$Latest"
      }

      override {
        instance_type     = "t3a.micro"
        weighted_capacity = "5"
      }

      override {
        instance_type     = "t3.micro"
        weighted_capacity = "2"
      }

      override {
        instance_type     = "t2.micro"
        weighted_capacity = "2"
      }
    }
  }
}
