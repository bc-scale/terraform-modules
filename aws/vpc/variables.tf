variable "cidr" {
  type        = string
  description = "vpc cidr"
}

variable "public_subnets" {
  type = list(object({
    az   = string
    cidr = string
  }))
  description = "map of az to public subnet cidr"
  default     = []
}

variable "private_subnets" {
  type = list(object({
    az   = string
    cidr = string
  }))
  description = "map of az to private subnet cidr"
  default     = []
}

variable "vpc_name" {
  type        = string
  description = "vpc name"
}

variable "bootstrap_bucket" {
  type        = string
  description = "bootstrap bucket name"
}
