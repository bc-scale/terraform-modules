resource "aws_vpc" "main" {
  cidr_block                       = var.cidr
  assign_generated_ipv6_cidr_block = true

  tags = merge(local.common_tags,
    {
      Name = var.vpc_name
    }
  )
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(local.common_tags,
    {
      Name = "${var.vpc_name} - igw"
    }
  )
}
