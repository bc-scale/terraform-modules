resource "aws_subnet" "private" {
  for_each = { for k, v in var.private_subnets : k => v }

  vpc_id                          = aws_vpc.main.id
  cidr_block                      = each.value.cidr
  availability_zone               = each.value.az
  map_public_ip_on_launch         = false
  ipv6_cidr_block                 = cidrsubnet(aws_vpc.main.ipv6_cidr_block, 8, index(var.private_subnets, each.value))
  assign_ipv6_address_on_creation = true

  tags = merge(local.common_tags,
    {
      Name = "${var.vpc_name}:${substr(each.value.az, -1, -1)} - private"
      type = "private"
    }
  )
}

resource "aws_subnet" "public" {
  for_each = { for k, v in var.public_subnets : k => v }

  vpc_id                          = aws_vpc.main.id
  cidr_block                      = each.value.cidr
  availability_zone               = each.value.az
  map_public_ip_on_launch         = true
  ipv6_cidr_block                 = cidrsubnet(aws_vpc.main.ipv6_cidr_block, 8, index(var.public_subnets, each.value) + length(var.private_subnets))
  assign_ipv6_address_on_creation = true

  tags = merge(local.common_tags,
    {
      Name = "${var.vpc_name}:${substr(each.value.az, -1, -1)} - public"
      type = "public"
    }
  )
}
