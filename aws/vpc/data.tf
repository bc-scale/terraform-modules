data "aws_ami" "amzn2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*"]
  }
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "nat" {
  statement {
    actions = [
      "s3:Get*",
    ]

    resources = [
      data.aws_s3_bucket.bootstrap.arn,
      "${data.aws_s3_bucket.bootstrap.arn}/*",
    ]
  }
}

data "aws_s3_bucket" "bootstrap" {
  bucket = var.bootstrap_bucket
}
