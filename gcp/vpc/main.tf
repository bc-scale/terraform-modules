resource "google_compute_network" "main" {
  name                    = var.vpc_name
  auto_create_subnetworks = false
  mtu                     = 1500
}

resource "google_compute_subnetwork" "main" {
  name          = var.vpc_name
  ip_cidr_range = "10.25.25.0/24"
  network       = google_compute_network.main.id
}

resource "google_compute_firewall" "main" {
  name    = var.vpc_name
  network = google_compute_network.main.name

  allow {
    protocol = "tcp"
    ports = [
      22,
    ]
  }
}
