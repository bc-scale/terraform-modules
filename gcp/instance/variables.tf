variable "name" {
  type        = string
  description = "instance name"
}

variable "machine_type" {
  type        = string
  description = "machine type"

  default = "f1-micro"
}

variable "zone" {
  type        = string
  description = "instance zone"
}

variable "subnetwork_name" {
  type        = string
  description = "subnetwork name"

  default = "waf"
}
