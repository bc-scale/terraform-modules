data "google_compute_image" "image" {
  family  = "fedora-coreos-testing"
  project = "fedora-coreos-cloud"
}

data "local_file" "ignition" {
  filename = "${path.module}/ignition.ign"
}
