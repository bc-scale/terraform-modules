resource "google_compute_instance" "main" {
  name                      = var.name
  machine_type              = var.machine_type
  zone                      = var.zone
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      size  = "50"
      type  = "pd-ssd"
      image = data.google_compute_image.image.self_link
    }
  }

  network_interface {
    subnetwork = var.subnetwork_name

    access_config {
      nat_ip = google_compute_address.main.address
    }
  }

  metadata = {
    user-data = data.local_file.ignition.content
  }
}

resource "google_compute_address" "main" {
  name = var.name
}
