variable "location" {
  type    = string
  description = "bucket location"

  default="US"
}

variable "bucket_name" {
  type        = string
  description = "bucket name"
}

variable "versioning" {
  type        = bool
  description = "enable versioning"

  default = false
}

variable "expire" {
  type        = bool
  description = "expire objects"

  default = true
}

variable "archive" {
  type        = bool
  description = "archive objects"

  default = true
}
