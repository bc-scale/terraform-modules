resource "google_storage_bucket" "main" {
  name = var.bucket_name

  storage_class = var.archive ? "ARCHIVE" : "STANDARD"
  location      = var.location

  dynamic "lifecycle_rule" {
    for_each = var.expire ? [1] : []

    content {
      action {
        type = "Delete"
      }

      condition {
        age        = var.archive ? 365 : 180
        with_state = "ANY"
      }
    }
  }

  dynamic "lifecycle_rule" {
    for_each = var.archive ? [1] : []

    content {
      action {
        type          = "SetStorageClass"
        storage_class = "ARCHIVE"
      }

      condition {
        age        = 10
        with_state = "ANY"
      }
    }
  }

  dynamic "versioning" {
    for_each = var.versioning ? [1] : []

    content {
      enabled = true
    }
  }
}
